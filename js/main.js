const head = document.head;
const link = document.createElement('link');
link.rel = 'stylesheet';

if (localStorage.getItem('themeStyle') === 'dark') {
    link.href = 'Style/style_v2.css';
} else {
    link.href = 'Style/style.css';
}

head.appendChild(link);

document.getElementById('btn-change').addEventListener('click', function () {

    if (localStorage.getItem('themeStyle') === 'light') {
        link.href = 'Style/style_v2.css';
        localStorage.setItem('themeStyle', 'dark');
    } else {
        link.href = 'Style/style.css';
        localStorage.setItem('themeStyle', 'light');
    }
});